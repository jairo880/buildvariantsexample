package com.jhonjairoduque.buildvariantsexample.Application;

import android.app.Application;
import android.content.Context;

/**
 * Created by jhonjairoduque on 2019-05-22.
 */

public class App extends Application {

    private Context context;

    static App instance;


    public static App getInstance() {
        if (instance == null) {
            instance = new App();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;


    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


}
