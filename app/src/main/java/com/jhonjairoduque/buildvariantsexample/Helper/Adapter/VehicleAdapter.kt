package com.jhonjairoduque.buildvariantsexample.Helper.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.jhonjairoduque.buildvariantsexample.Application.App
import com.jhonjairoduque.buildvariantsexample.Model.Vehicle
import com.jhonjairoduque.buildvariantsexample.R

/**
 * Created by jhonjairoduque on 2019-05-25.
 */


class VehicleAdapter(private val vehicles: List<Vehicle>, private val onItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<VehicleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_view_vehicle, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val vehicle = vehicles[position]

        inflateView(vehicle, holder)

        addListenersItem(position, holder)
    }

    private fun inflateView(vehicle: Vehicle, holder: ViewHolder) {

        setInformationVehicle(holder, vehicle)
        loadImageVehicle(vehicle, holder)
        addListeners(holder, vehicle)
    }

    private fun addListeners(holder: ViewHolder, vehicle: Vehicle) {
        holder.buttonMoreInformation.setOnClickListener {
            onItemClickListener.onItemClick(vehicle)
        }
    }

    private fun loadImageVehicle(
        vehicle: Vehicle,
        holder: ViewHolder
    ) {
        val requestManager = Glide.with(App.getInstance().context)
        val requestBuilder = requestManager.load(vehicle.urlImagen)
        requestBuilder.into(holder.imageViewVehicle)
    }

    private fun setInformationVehicle(
        holder: ViewHolder,
        vehicle: Vehicle
    ) {
        holder.textViewVehicleName.text = vehicle.name
        holder.textViewVehicleModel.text = vehicle.modelo
        holder.ratingBar.numStars = vehicle.calificacion
        holder.textViewCostCK.text = "$" + vehicle.ck.toString()
        holder.textViewCostKL.text = "$" + vehicle.kl.toString()
    }

    private fun addListenersItem(position: Int, holder: ViewHolder) {
        holder.contentVehicle.setOnClickListener { onItemClickListener.onItemClick(vehicles[position]) }
    }

    //    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    //    private void rippleEffectBackground(ViewHolder viewHolder) {
    //        viewHolder.contentItemIssueMaitenanceRecent.setBackgroundResource(R.drawable.ripple_efect_selected);
    //    }

    override fun getItemCount(): Int {
        return vehicles.size
    }

    //    public void clearData() {
    //        if (IssueMaintenanceRecents != null) {
    //            IssueMaintenanceRecents.clear();
    //            notifyDataSetChanged();
    //        }
    //    }
    //
    //    public List<IssueMaintenanceRecent> filter(String charTextSequence) {
    //        if (tmpListIssueMaintenanceRecents != null) {
    //            tmpListIssueMaintenanceRecents.clear();
    //        }
    //        charTextSequence = charTextSequence.toLowerCase(Locale.getDefault());
    //        for (IssueMaintenanceRecent IssueMaintenanceRecent : IssueMaintenanceRecents) {
    //            if (String.valueOf(IssueMaintenanceRecent.getNumber()).toLowerCase(Locale.getDefault()).contains(charTextSequence)) {
    //                tmpListIssueMaintenanceRecents.add(IssueMaintenanceRecent);
    //            }
    //        }
    //        return tmpListIssueMaintenanceRecents;
    //    }

    interface OnItemClickListener {

        fun onItemClick(vehicle: Vehicle)

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val textViewVehicleName: TextView = itemView.findViewById(R.id.text_view_vehicle_name)
        val textViewVehicleModel: TextView = itemView.findViewById(R.id.text_view_vehicle_model)
        val ratingBar: RatingBar = itemView.findViewById(R.id.rating_bar_vehicle)
        val contentVehicle: LinearLayout = itemView.findViewById(R.id.layout_content_vehicle_item)
        val buttonMoreInformation: LinearLayout = itemView.findViewById(R.id.button_more_information_vehicle)
        val textViewCostCK: TextView = itemView.findViewById(R.id.text_view_ck_vehicle_cost)
        val textViewCostKL: TextView = itemView.findViewById(R.id.text_view_kl_vehicle_cost)
        val imageViewVehicle: ImageView = itemView.findViewById(R.id.image_view_vehicle)

    }
}
