package com.jhonjairoduque.buildvariantsexample.Rest

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

/**
 * Created by jhon.duque on 2019-06-05.
 */

class VolleyManager private constructor(context: Context) {
    private var mRequestQueue: RequestQueue? = null

    // getApplicationContext() is key, it keeps you from leaking the
    // Activity or BroadcastReceiver if someone passes one in.
    val requestQueue: RequestQueue
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(mCtx.applicationContext)
            }
            return this.mRequestQueue!!
        }

    init {
        mCtx = context
        mRequestQueue = requestQueue
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        requestQueue.add(req)
    }

    companion object {

        private var mInstance: VolleyManager? = null
        private lateinit var mCtx: Context

        @Synchronized
        fun getInstance(context: Context): VolleyManager {
            if (mInstance == null) {
                mInstance = VolleyManager(context)
            }
            return mInstance as VolleyManager
        }
    }
}
