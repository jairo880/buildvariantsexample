package com.jhonjairoduque.buildvariantsexample.Rest

import com.google.gson.Gson

/**
 * Created by jhon.duque on 2019-06-05.
 */


object VolleyUtils {

    fun getJsonObjet(objet: String): String {
        return Gson().toJson(objet)
    }

}
