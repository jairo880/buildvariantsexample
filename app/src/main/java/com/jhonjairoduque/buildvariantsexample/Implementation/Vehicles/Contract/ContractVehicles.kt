package com.jhonjairoduque.buildvariantsexample.Implementation.Vehicles.Contract

import com.jhonjairoduque.buildvariantsexample.Model.Vehicle

/**
 * Created by jhon.duque on 2019-06-05.
 */

class ContractVehicles {


    interface view {

        fun showMessage(message: String)

        fun showVehicles(vehicle: List<Vehicle>)
    }

    interface presenter {

        fun getVehicles()

        fun retrieveVehicles(vehicles: List<Vehicle>)

        fun errorGetVehicles(message: String)

    }


}