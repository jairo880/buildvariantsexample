package com.jhonjairoduque.buildvariantsexample.Implementation.Vehicles.Repository

import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jhonjairoduque.buildvariantsexample.Application.App
import com.jhonjairoduque.buildvariantsexample.BuildConfig
import com.jhonjairoduque.buildvariantsexample.Implementation.Vehicles.Contract.ContractVehicles
import com.jhonjairoduque.buildvariantsexample.Model.Vehicle
import com.jhonjairoduque.buildvariantsexample.R
import com.jhonjairoduque.buildvariantsexample.Rest.VolleyQueueManager

/**
 * Created by jhon.duque on 2019-06-05.
 */

class RepositoryVehicle {

    fun fetchVehicles(handler: ContractVehicles.presenter) {


        var url =  BuildConfig.URL_API_BASE + BuildConfig.URL_API_END_POINT

        val request = object : JsonArrayRequest(
            Request.Method.GET, url,
            Response.Listener { response ->
                val vehicleListType = object : TypeToken<List<Vehicle>>() {}.type

                handler.retrieveVehicles(Gson().fromJson(response.toString(), vehicleListType))
            },
            Response.ErrorListener {
                handler.errorGetVehicles(App.getInstance().context.getString(R.string.message_error_generic))
            }) {
        }

        request.retryPolicy = DefaultRetryPolicy(
            20000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        VolleyQueueManager.getInstance(App.getInstance().context).addToRequestQueue(request)

    }
}