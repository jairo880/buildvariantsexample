package com.jhonjairoduque.buildvariantsexample.Implementation.Vehicles

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.jhonjairoduque.buildvariantsexample.Application.App
import com.jhonjairoduque.buildvariantsexample.Helper.Adapter.VehicleAdapter
import com.jhonjairoduque.buildvariantsexample.Implementation.Vehicles.Contract.ContractVehicles
import com.jhonjairoduque.buildvariantsexample.Implementation.Vehicles.Presenter.PresenterVehicles
import com.jhonjairoduque.buildvariantsexample.Model.Vehicle
import com.jhonjairoduque.buildvariantsexample.R
import kotlinx.android.synthetic.main.activity_main.*

class VehiclesActivity : AppCompatActivity(), ContractVehicles.view, VehicleAdapter.OnItemClickListener {

    private var presenter: ContractVehicles.presenter? = null
    private var adapterVehicles: VehicleAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = PresenterVehicles(this)

    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showVehicles(vehicles: List<Vehicle>) {
        adapterVehicles = VehicleAdapter(vehicles, this)
        recycler_view_vehicles.adapter = adapterVehicles
        recycler_view_vehicles.setHasFixedSize(true)
        recycler_view_vehicles.isNestedScrollingEnabled = false
    }

    override fun onResume() {
        super.onResume()

        App.getInstance().context = this
        presenter?.getVehicles()
    }

    override fun onItemClick(vehicle: Vehicle) {
        Toast.makeText(this, vehicle.name, Toast.LENGTH_SHORT).show()
    }


}
