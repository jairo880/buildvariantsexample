package com.jhonjairoduque.buildvariantsexample.Implementation.Vehicles.Presenter

import com.jhonjairoduque.buildvariantsexample.Implementation.Vehicles.Contract.ContractVehicles
import com.jhonjairoduque.buildvariantsexample.Implementation.Vehicles.Repository.RepositoryVehicle
import com.jhonjairoduque.buildvariantsexample.Model.Vehicle

/**
 * Created by jhon.duque on 2019-06-05.
 */

class PresenterVehicles(private var view: ContractVehicles.view) : ContractVehicles.presenter {


    private var repository: RepositoryVehicle = RepositoryVehicle()

    override fun retrieveVehicles(vehicles: List<Vehicle>) {
        view.showVehicles(vehicles)
    }

    override fun getVehicles() {
        repository.fetchVehicles(this)
    }

    override fun errorGetVehicles(message: String) {
        view.showMessage(message)
    }

}