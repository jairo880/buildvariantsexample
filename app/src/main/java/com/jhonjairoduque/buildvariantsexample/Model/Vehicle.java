package com.jhonjairoduque.buildvariantsexample.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vehicle {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("Calificacion")
    @Expose
    private Integer calificacion;
    @SerializedName("CK")
    @Expose
    private String cK;
    @SerializedName("KL")
    @Expose
    private String kL;
    @SerializedName("URLImagen")
    @Expose
    private String uRLImagen;
    @SerializedName("Modelo")
    @Expose
    private String modelo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public String getCK() {
        return cK;
    }

    public void setCK(String cK) {
        this.cK = cK;
    }

    public String getKL() {
        return kL;
    }

    public void setKL(String kL) {
        this.kL = kL;
    }

    public String getURLImagen() {
        return uRLImagen;
    }

    public void setURLImagen(String uRLImagen) {
        this.uRLImagen = uRLImagen;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

}